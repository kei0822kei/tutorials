<?php

class Entree {
  private $name;
  protected $ingredients = array();

  public function getName() {
    return $this->name;
  }

  public function __construct($name, $ingredients) {
    if (! is_array($ingredients)) {
      throw new Exception('$ingredients must be an array');
    }
    $this->name = $name;
    $this->ingredients = $ingredients;
  }

  public function hasIngredient($ingredient) {
    return in_array($ingredient, $this->ingredients);
  }

  public static function getSizes() {
    return array('small', 'medium', 'large');
  }
}

$soup = new Entree('Chicken Soup', array('chicken', 'water'));
$sandwich = new Entree('Chicken Sandwich', array('chicken', 'bread'));

foreach (['chicken', 'lemon', 'bread', 'water'] as $ing) {
  if ($soup->hasIngredient($ing)) {
    print "Soup contains $ing.\n";
  }
  if ($sandwich->hasIngredient($ing)) {
    print "Sandwich contains $ing.\n";
  }
}

try {
  $drink = new Entree('Glass of Milk', 'milk');
  if ($drink->hasIngredient('milk')) {
    print "Yummy!";
  }
} catch (Exception $e) {
  print "Couldn't create the drink: " . $e->getMessage() . "\n";
}


class ComboMeal extends Entree {

  public function __construct($name, $entrees) {
    parent::__construct($name, $entrees);
    foreach ($entrees as $entree) {
      if (! $entree instanceof Entree) {
        throw new Exception('Elements of $entrees must be Entree objects');
      }
    }
  }

  public function hasIngredient($ingredient) {
    foreach ($this->ingredients as $entree) {
      if ($entree->hasIngredient($ingredient)) {
        return true;
      }
    }
    return false;
  }
}

$soup = new Entree('Chicken Soup', array('chicken', 'water'));
$sandwich = new Entree('Chicken Sandwich', array('chicken', 'bread'));

$combo = new ComboMeal('Soup + Sandwich', array($soup, $sandwich));

foreach (['chicken', 'water', 'pickles'] as $ing) {
  if ($combo->hasIngredient($ing)) {
    print "Something in the combo contains $ing.\n";
  }
}
