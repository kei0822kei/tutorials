<?php
$cardOppFace = $_POST['leftCardFace'];
$cardOppSuit = $_POST['leftCardSuit'];
$cardOppKey = $_POST['leftCardKey'];
$cardPlayerFace = $_POST['rightCardFace'];
$cardPlayerSuit = $_POST['rightCardSuit'];
$cardPlayerKey = $_POST['rightCardKey'];
$highOrLow = $_POST['select'];
?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width" />
<title>High & low game</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div align="center">
<h1>High & low game</h1>
<hr>
<?php
echo <<< EOM
Opp card:
{$cardOppSuit}{$cardOppFace}<br>
You chose ${highOrLow} <br> <br>
Your card:
{$cardPlayerSuit}{$cardPlayerFace}<br>
EOM;

if($cardOppKey < $cardPlayerKey){
  $result = "High";
}elseif($cardOppKey > $cardPlayerKey){
  $result = "Low";
}else{
  $result = $hightOrLow;
}

if($highOrLow == $result){
  echo 'You win!';
}else{
  echo 'You lose!';
}
?>
<br>
<a href="index.php">try again</a>
</div>
</body>
</html>
