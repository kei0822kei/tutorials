/* 11. 各グループごとの総得点数を表示して下さい。 */

with A as (
  select id, my_country_id
  from pairings
  where kickoff between '2014-06-13 05:00:00' and '2014-06-27 23:59:59'  -- 予選の期間
  ), B as (
  select pairing_id, count(pairing_id) as goal_num
  from goals
  group by pairing_id
  ), D as (
  select A.*, B.*, C.group_name as group_name
  from A
  join B
  on A.id = B.pairing_id
  left join countries as C
  on A.my_country_id = C.id
  )

select D.group_name, sum(goal_num) as goal_num
from D
group by group_name;


/* group league is held from 2014-06-13 05:00:00 to 2014-06-27 23:59:59 */
