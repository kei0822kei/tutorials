/* 5. キックオフ日時と対戦国の国名をキックオフ日時の早いものから順に表示してください。 */

select A.kickoff,
       B.name as my_country_name,
       C.name as enemy_country_name
from pairings as A
left join countries as B
on B.id = A.my_country_id
left join countries as C
on C.id = A.enemy_country_id
