/* 2. 全ゴールキーパーの平均身長、平均体重を表示してください。 */ 

select avg(height) as '平均身長',
       avg(weight) as '平均体重'
from players
where position = 'GK'
