/* 7. すべての選手を対象として選手ごとの得点ランキングを表示してください。（テーブル結合を使うこと）*/

with A as (
  select player_id, count(player_id) as goal_num
  from goals
  group by player_id
  )

select B.name, B.club, ifnull(A.goal_num, 0) as goal_num
from players as B
left join A
on B.id = A.player_id
order by goal_num desc
