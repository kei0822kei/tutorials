/* 6. すべての選手を対象として選手ごとの得点ランキングを表示してください。（SELECT句で副問合せを使うこと）*/

select B.name, B.club, ifnull(A.goal_num, 0) as goal_num
from players as B
left join (
  select player_id, count(player_id) as goal_num
  from goals
  group by player_id
  ) as A
on B.id = A.player_id
order by goal_num desc
