/* 14. グループCの各対戦毎にゴール数を表示してください。（外部結合で）*/

with L as (
  select A.id as pairing_id,
         B.id as my_country_id,
         B.name as my_country_name,
         C.id as enemy_country_id,
         C.name as enemy_country_name
  from pairings as A
  left join countries as B
  on A.my_country_id = B.id
  left join countries as C
  on A.enemy_country_id = C.id
  where B.group_name = 'C'
    and C.group_name = 'C'  -- B も C もグループ C ということは予選である
  ), M as (
  select pairing_id, count(pairing_id) as goal_num
  from goals
  group by pairing_id
  )

/* right join L */
/* on M.pairing_id = L.pairing_id */
select L.*, ifnull(M.goal_num, 0) as goal_num
from L
left join M
on L.pairing_id = M.pairing_id
