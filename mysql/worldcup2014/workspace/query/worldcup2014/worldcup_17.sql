/* 17. 問題16の結果に得失点差を追加してください。 */

select 
  pa.kickoff, 
  my_c.name as my_country, 
  en_c.name as enemy_country,
  my_c.ranking as my_ranking, 
  en_c.ranking as enemy_ranking,
  (
    select count(g.id)
    from goals g
    where pa.id = g.pairing_id
  ) as my_goals,
  (
    select count(g2.id)
    from goals g2
    left join pairings pa2
      on pa2.id = g2.pairing_id
    where 1=1
      and pa2.my_country_id = pa.enemy_country_id
      and pa2.enemy_country_id = pa.my_country_id
  ) as enemy_goals,
--    my_goals - enemy_goals as goal_diff -- こうしたいが、できない。(UnknownColumn)
  (
    select count(g.id)
    from goals g
    where pa.id = g.pairing_id
  ) - (
    select count(g2.id)
    from goals g2
    left join pairings pa2
      on pa2.id = g2.pairing_id
    where 1=1
      and pa2.my_country_id = pa.enemy_country_id
      and pa2.enemy_country_id = pa.my_country_id
  ) as goal_diff -- 冗長だがこうするしかない。
from pairings as pa
left join countries my_c
  on pa.my_country_id = my_c.id
left join countries en_c
  on pa.enemy_country_id = en_c.id
where 1=1
  and my_c.group_name = 'C'
  and en_c.group_name = 'C'
order by
  pa.kickoff,
  my_c.ranking
;
