/* 15. グループCの各対戦毎にゴール数を表示してください。（サブクエリで）*/

select L.*, ifnull(M.goal_num, 0) as goal_num
from (
  select A.id as pairing_id,
         B.id as my_country_id,
         B.name as my_country_name,
         C.id as enemy_country_id,
         C.name as enemy_country_name
  from pairings as A
  left join countries as B
  on A.my_country_id = B.id
  left join countries as C
  on A.enemy_country_id = C.id
  where B.group_name = 'C'
    and C.group_name = 'C'  -- B も C もグループ C ということは予選である
) as L
left join (
  select pairing_id, count(pairing_id) as goal_num
  from goals
  group by pairing_id
) as M
on L.pairing_id = M.pairing_id
