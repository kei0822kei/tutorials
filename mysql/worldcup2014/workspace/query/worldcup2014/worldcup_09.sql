/* 9. ワールドカップ開催当時（2014-06-13）の年齢をプレイヤー毎に表示する。 */

select birth, timestampdiff(year, birth, '2014-06-13') as age, name, position
from players
order by age desc
