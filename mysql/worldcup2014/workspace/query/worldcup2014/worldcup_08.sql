/* 8. 各ポジションごとの総得点を表示してください。*/

select B.position, count(A.id) as goal_num
from goals as A
left join players as B
on A.player_id = B.id
group by position
order by goal_num desc

/* position NULL -> own goal */
/* if you use B.id substitute for A.id null record is ignored */
