/* 4. 各国の平均身長を高い方から順に表示してください。ただし、FROM句はplayersテーブルとして、テーブル結合を使わず副問合せを用いてください。 */

select B.name as country_name, A.*
from countries as B
left join (
  select country_id, avg(height) as ave_height
  from players
  group by country_id
  ) as A
on B.id = A.country_id
order by A.ave_height desc
