#!/usr/bin/env python

"""
command example: python mysql_sample.py --host 172.19.0.2 --password mysql
"""

import argparse
from pymysql import connect, cursors


def get_argparse():
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--db', type=str, default='world',
                        help="Database name.")
    parser.add_argument('--host', type=str,
                        help="Host name or ip address.")
    parser.add_argument('--port', type=int, default=3306,
                        help="Port.")
    parser.add_argument('--user', type=str, default='root',
                        help="User name.")
    parser.add_argument('--password', type=str, default='mysql',
                        help="Password.")
    arguments = parser.parse_args()

    return arguments


def main(host, user, db, port, password):
    conn = connect(host=host,
                   user=user,
                   db=db,
                   port=port,
                   password=password)
    try:
        with conn.cursor() as cursor:
            sql = "SELECT * FROM city LIMIT 10"
            cursor.execute(sql)
            result = cursor.fetchall()
            print(type(result))
            print()
            print(result)
    finally:
        conn.close()


if __name__ == '__main__':
    args = get_argparse()
    main(host=args.host,
         user=args.user,
         db=args.db,
         port=args.port,
         password=args.password)
