# Evoke

```sh
docker-compose up -d
```

進め方としては、とりあえずサイトの答えをコピペしてから、
mysql 5.6 -> mysql 8.0 に自力で書き換えていく。

# References

[question and data source](https://tech.pjin.jp/blog/2016/12/05/sql%e7%b7%b4%e7%bf%92%e5%95%8f%e9%a1%8c-%e4%b8%80%e8%a6%a7%e3%81%be%e3%81%a8%e3%82%81/)
[sample answers](https://zenn.dev/itoo/articles/sql-practice)
