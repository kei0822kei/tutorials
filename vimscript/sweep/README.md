# sweep

Plugin sweep is vim plugin sample which remove white space
of each line.


## how to use

- add plugin in path to runtimepath
```text:.vimrc
set runtimepath ^= /path/to/sweep
```

- evoke vim and add command as bellow
```text
:autocmd plugin-sweep_trail
:let g:sweep_trail#enable = 1
```
