let s:save_cpo = &cpo
set cpo&vim

function! sweep_trail#sweep()
  %substitute/\s\+$//
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo

function sweep_trail#auto_sweep()
  if g:sweep_trail#enable
    call sweep_trail#sweep()
  endif
endfunction
