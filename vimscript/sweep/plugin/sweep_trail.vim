if exists('g:loaded_sweep_trail')
  finish
endif
let g:loaded_sweep_trail = 1

let s:save_cpo = &cpo
set cpo&vim

let &cpo = s:save_cpo
unlet s:save_cpo

augroup plugin-sweep_trail
  autocmd!
  autocmd BufWritePre * call sweep_trail#auto_sweep()
augroup END
