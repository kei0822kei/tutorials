#include <stdio.h>

/* __global__
 * は、関数をCPUから呼び出し、GPUで実行されることをコンパイラに認識させる */
__global__ void helloFromGPU() { printf("Hello World from GPU!\n"); }

int main(int argc, char **argv) {
  printf("Hello World from CPU!\n");

  /* <<<...>>>
   * は、ホストスレッドからのデバイスコードの呼び出しを指定する。カーネルは一連のスレッドによって実行される。
   */
  /* この例では、GPU スレッドを10個実行することになる。 */
  helloFromGPU<<<1, 10>>>();
  cudaDeviceReset();
  return 0;
}
