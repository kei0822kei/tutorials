/* 友愛数 */

#include <iostream>
#include "sample_006.hpp"

void print_amicable(int const limit)
{
  for (int number = 4; number < limit; ++number)
  {
    if (auto sum1 = sum_proper_divisors(number); sum1 < limit)
    {
      if (auto sum2 = sum_proper_divisors(sum1); sum2 == number && number != sum1)
      {
        std::cout << number << "," << sum1 << std::endl;
      }
    }
  }
}

int main()
{
  int limit = 0;
  std::cout << "Upper limit:";
  std::cin >> limit;

  print_amicable(limit);
}
