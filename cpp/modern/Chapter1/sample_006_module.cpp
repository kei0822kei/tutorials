/* 過剰数をもとめる。過剰数とは、真の約数の総和が元の数よりも大きくなる数である。 */

#include <iostream>
#include <cmath>  // std::sqrt を使用するために必要

int sum_proper_divisors(int const number)
{
  int result = 1;
  int const root = static_cast<int>(std::sqrt(number));
  for (int i = 2; i <= root; i++)
  {
    if (number % i == 0)
    {
      result += (i == (number / i)) ? i : (i + number / i); // 25/5=5 のときは 5 を加えることになる
    }
  }
  return result;
}
