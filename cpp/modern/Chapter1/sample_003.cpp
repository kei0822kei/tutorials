/* 最小公倍数 */

#include <iostream>
#include "sample_002.hpp"

/* unsigned int gcd(unsigned int a, unsigned int b) { */
/*   unsigned int r; */
/*   while (b != 0) { */
/*     r = a % b; */
/*     a = b; */
/*     b = r; */
/*   } */
/*   return a; */
/* } */

unsigned int lcm(const unsigned int a, const unsigned int b) {
  unsigned int h = gcd(a, b);
  return h ? (a * b / h) : 0;  // return a * b / h でよくない？
}

int main() {
  int a = 20;
  int b = 15;
  int c;
  c = lcm(a, b);
  std::cout << c << std::endl;
}
