/* 過剰数をもとめる。過剰数とは、真の約数の総和が元の数よりも大きくなる数である。 */

#include <iostream>
#include <cmath>  // std::sqrt を使用するために必要

int sum_proper_divisors(int const number)
{
  int result = 1;
  int const root = static_cast<int>(std::sqrt(number));
  for (int i = 2; i <= root; i++)
  {
    if (number % i == 0)
    {
      result += (i == (number / i)) ? i : (i + number / i); // 25/5=5 のときは 5 を加えることになる
    }
  }
  return result;
}

void print_abundant(int const limit)
{
  for (int number = 1; number <= limit; ++ number)
  {
    if (auto sum = sum_proper_divisors(number); sum > number)  // auto は型推論
    {
      std::cout << number
                << ", abundance=" << sum - number << std::endl;  // abundance
    }
  }
}

int main()
{
  int limit = 0;
  std::cout << "Upper limit:";
  std::cin >> limit;

  print_abundant(limit);
}
