/* Run command: g++ sample_005.cpp sample_004_module.cpp */

#include <iostream>
#include "sample_004.hpp"

int main()
{
  int limit = 0;
  std::cout << "Upper limit: ";
  std::cin >> limit;

  for (int n = 2; n <= limit; n++)
  {
    if (is_prime(n) && is_prime(n + 6))
    {
      std::cout << n << "," << n + 6 << std::endl;
    }
  }
}
