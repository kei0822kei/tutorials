/* 最大公約数 */

#include <iostream>

#include "002_module.hpp"

int main() {
  int a = 20;
  int b = 15;
  int c;
  c = gcd(a, b);
  std::cout << c << std::endl;
}
