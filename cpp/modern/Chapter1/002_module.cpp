unsigned int gcd(unsigned int a, unsigned int b) {
  unsigned int r;
  while (b != 0) {
    r = a % b;
    a = b;
    b = r;
  }
  return a;
}
