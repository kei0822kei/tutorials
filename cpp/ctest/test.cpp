#include "hello.hpp"
#include <iostream>
#include <cassert>

int main() {
    Hello h;
    //assert(h.hello(nullptr) == ""); //This will be error, therefore commented out.
    assert(h.hello() == "empty");
    assert(h.hello("") == "empty");
    assert(h.hello("John Doe") == "Hello John Doe");
    std::cout << "done" << std::endl;
    return 0;
}
