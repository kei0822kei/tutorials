#ifndef HELLO_HPP
#define HELLO_HPP

#include <string>

class Hello {
public:
    std::string hello(const char* const p = "") const;
};

#endif //HELLO_HPP
