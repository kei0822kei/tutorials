/* 1.1 重複のない文字列 */
/* ある文字列が、全て固有である（重複する文字がない）かどうかを判定するアルゴリズムを実装してください。 */
/* また、それを実装するのに新たなデータ構造が使えない場合、どのようにすればよいですか？ */

#include<iostream>

// ASCII コードは 7 bit
// 拡張ASCII コードは 8 bit
// ref: https://www.asciim.cn/jp/

// 文字コードが(拡張)ASCIIの場合
bool UniqueExtendedAscii(std::string s) {
  bool    f[256];

  for (int i = 0; i < s.length(); i++) {
    int v = (int)(s[i]);
    if (f[v]) {
      return false;
    }
    f[v] = true;
  }
  return true;
}

//英小文字26文字の場合
bool UniqueOrNot(std::string s) {
  int v = 0, a;
  for (int i = 0; i < s.length(); i++) {
    a = (int)(s[i] - 'a');
    if ((v & (1 << a)) > 0) {
      return false;
    }
    v |= (1 << a);
  }
  return true;
}

int main() {
  bool hoge;
  std::string s = "abcdd";
  hoge = UniqueExtendedAscii(s);
  std::cout << hoge << std::endl;
}

