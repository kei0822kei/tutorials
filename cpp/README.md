# Setup

ref: https://www.xn--xcktavb1xc.jp/blog/installation-clang-ubuntu

## Evoke Docker Container for C++

```sh
docker-compose up -d
```


## Get in the container

```sh
docker exec -it -u keiyu cpp-clang zsh  # -u keiyu をつけないと root になれない。詳細不明。
```


## Neovim Settings

```vim
:CocInstall coc-clangd
```


## Check Default Include Path

```sh
clang -x c++ -v -E /dev/null
```

ref: https://jnsato.hateblo.jp/entry/2021/01/10/230000
