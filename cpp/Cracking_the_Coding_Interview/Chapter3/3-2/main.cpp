#include <iostream>

class MinStack {
  public:
  typedef struct node {
    int v;
    int minUntilNow;
    node* next;
  } Node;

  MinStack() : topN(nullptr) {}

  void push(int val) {
    Node* n = new Node;
    n->v = n->minUntilNow = val;
    n->next = nullptr;

    if (topN == nullptr) {
      topN = n;
    } else {
      n->minUntilNow = std::min(n->v, topN->minUntilNow);
      n->next = topN;
      topN = n;
    }
  }

  void pop() { topN = topN->next; }

  int top() { return topN->v; }

  int getMin() { return topN->minUntilNow; }

  private:
  Node* topN;
};

int main() {
  MinStack *stack = new MinStack();
  stack->push(3);
  stack->push(10);
  stack->push(1);
  stack->push(4);
  stack->push(-1);
  stack->pop();
  std::cout << "Top value: " << stack->top() << std::endl;
  std::cout << "Min value: " << stack->getMin() << std::endl;
}
