#include <bitset>
#include <iostream>
#include <string>
#include <vector>

bool isUniqueChars(const std::string &str) {
  if (str.length() > 128) {
    return false;
  }
  std::vector<bool> char_set(128);
  for (int i = 0; i < str.length(); i++) {
    int val = str[i];
    if (char_set[val]) {
      return false;
    }
    char_set[val] = true;
  }
  return true;
}

bool isUniqueChars_bitvector(const std::string &str) {
  // Reduce space usage by a factor of 8 using bitvector.
  // Each boolean otherwise occupies a size of 8 bits.
  std::bitset<256> bits(0);
  for (int i = 0; i < str.length(); i++) {
    int val = str[i];
    if (bits.test(val) > 0) {
      return false;
    }
    bits.set(val);
  }
  return true;
}

bool isUniqueChars_noDS(const std::string &str) {
  for (int i = 0; i < str.length() - 1; i++) {
    for (int j = i + 1; j < str.length(); j++) {
      if (str[i] == str[j]) {
        return false;
      }
    }
  }
  return true;
}

int main() {
  std::vector<std::string> words = {"abcde", "hello", "apple", "kite", "padle"};
  for (auto word : words) {
    std::cout << word << std::string(": ") << std::boolalpha
              << isUniqueChars(word) << std::endl;
  }
  std::cout << std::endl << "Using bit vector" << std::endl;
  for (auto word : words) {
    std::cout << word << std::string(": ") << std::boolalpha
              << isUniqueChars_bitvector(word) << std::endl;
  }
  std::cout << std::endl << "Using no Data Structures" << std::endl;
  for (auto word : words) {
    std::cout << word << std::string(": ") << std::boolalpha
              << isUniqueChars_bitvector(word) << std::endl;
  }
  return 0;
}
