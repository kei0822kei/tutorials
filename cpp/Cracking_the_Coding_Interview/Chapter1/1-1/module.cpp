/* 1.1 重複のない文字列 */
/* ある文字列が、全て固有である（重複する文字がない）かどうかを判定するアルゴリズムを実装してください。
 */
/* また、それを実装するのに新たなデータ構造が使えない場合、どのようにすればよいですか？
 */

#include "module.hpp"

#include <string>

// ASCII コードは 7 bit
// 拡張ASCII コードは 8 bit
// ref: https://www.asciim.cn/jp/

// 文字コードが(拡張)ASCIIの場合
bool is_unique_exascii(std::string s) {
  bool f[256] = {false};
  for (int i = 0; i < s.length(); i++) {
    int v = (int)(s[i]);
    if (f[v]) {
      return false;
    }
    f[v] = true;
  }
  return true;
}

//英小文字26文字の場合
bool is_unique_lowerchar(std::string s) {
  int v = 0, a;
  for (int i = 0; i < s.length(); i++) {
    // たとえば s[i] = 'f' であれば a=102-97=5 となる。
    a = (int)(s[i] - 'a');
    // a=5 のとき、 1 << a は 1 を 5bit シフトさせるので 32bit の末尾が 00000001
    // -> 00100000 となる。 英小文字の重複判定用に 26bit
    // 容易しておいて、１回でも登場した文字は 1 をたてる。
    if ((v & (1 << a)) > 0) {
      return false;
    }
    v |= (1 << a);
  }
  return true;
}
