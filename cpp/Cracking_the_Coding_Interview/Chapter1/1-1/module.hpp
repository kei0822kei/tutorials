#ifndef MODULE_HPP
#define MODULE_HPP

#include <string>

bool is_unique_exascii(std::string s);
bool is_unique_lowerchar(std::string s);

#endif
