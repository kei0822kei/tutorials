#include <gtest/gtest.h>

#include "module.hpp"

TEST(BasicTest, TestStringToInt) { EXPECT_EQ(int('a'), 97); }

TEST(ModuleTest, TestUniqueExASCII) {
  EXPECT_TRUE(is_unique_exascii("abcde"));
  EXPECT_FALSE(is_unique_exascii("abcdd"));
  /* EXPECT_TRUE(is_unique_exascii("abcde€")); // TODO: € を utf-8 ではなく
   * ascii でデコードしなければならないのでは？ */
  /* EXPECT_FALSE(is_unique_exascii("abcde€€")); */
}

TEST(ModuleTest, TestUniqueLowerCharacter) {
  EXPECT_TRUE(is_unique_exascii("abcde"));
  EXPECT_FALSE(is_unique_exascii("abcdd"));
}
