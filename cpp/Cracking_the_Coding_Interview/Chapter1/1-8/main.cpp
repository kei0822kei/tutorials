#include <iostream>

#include "module.hpp"

int main() {
  int M = 4;
  int N = 6;
  int **matrix = new int *[M];
  for (int i = 0; i < N; ++i) {
    matrix[i] = new int[N];
  };
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      matrix[i][j] = 1;
    }
  }
  matrix[0][2] = 0;
  matrix[0][4] = 0;
  matrix[1][0] = 0;
  matrix[1][6] = 0;
  matrix[3][0] = 0;
  matrix[3][3] = 0;
  /* int matrix[4][6] = { */
  /*   {1, 1, 0, 1, 0, 1}, */
  /*   {0, 1, 1, 1, 1, 0}, */
  /*   {1, 1, 1, 1, 1, 1}, */
  /*   {0, 1, 1, 0, 1, 1}, */
  /* }; */
  printMatrix(matrix, M, N);
  nullifyMatrix(matrix, M, N);
  printMatrix(matrix, M, N);
}
