#ifndef MODULE_HPP
#define MODULE_HPP

void nullifyMatrix(int** matrix, int M, int N);
void printMatrix(int** matrix, int M, int N);

#endif
