/* 1.2 順列チェック */
/* ２つの文字列が与えられたとき、片方がもう片方の並び替えになっているかどうかを決定するメソッドを書いてください。
 */

#include <algorithm>
#include <string>

bool is_permutation_unique(std::string s, std::string t) {
  sort(s.begin(), s.end());
  sort(t.begin(), t.end());
  return s == t;
}

bool is_permutation_unique_custom(std::string s, std::string t) {
  int let[256] = {0};
  for (int i = 0; i < s.length(); i++) {
    let[s[i]]++;
  }
  for (int i = 0; i < s.length();
       i++) {  // t.length() だとだめで s.length() にする必要がありそう
    let[t[i]]--;
    if (let[t[i]] < 0) {
      return false;
    }
  }
  return true;
}
