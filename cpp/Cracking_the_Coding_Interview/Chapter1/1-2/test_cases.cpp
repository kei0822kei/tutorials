#include <gtest/gtest.h>
#include "module.hpp"
#include <iostream>

TEST(ModuleTest, IsPermutationUnique) {
  EXPECT_TRUE(is_permutation_unique("dog", "god"));
  EXPECT_FALSE(is_permutation_unique("dog", "godd"));
}

TEST(ModuleTest, IsPermutationUniqueCustom) {
  EXPECT_TRUE(is_permutation_unique_custom("dog", "god"));
  EXPECT_FALSE(is_permutation_unique("dog", "godd"));
}
