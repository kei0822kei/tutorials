#ifndef MODULE_HPP
#define MODULE_HPP

#include <string>

bool is_permutation_unique(std::string s, std::string t);
bool is_permutation_unique_custom(std::string s, std::string t);

#endif
