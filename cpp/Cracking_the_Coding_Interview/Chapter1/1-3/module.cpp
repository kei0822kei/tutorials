/* 1.3 URLify */
/* 文字列内に出現する全ての空白文字を "%20" で置き換えるメソッドを書いてください。 */
/* ただし、文字列の後ろには新たに文字を追加するためのスペースが十分にある（バッファのサイズはきにしなくてもよい）ことと、その追加用スペースを除いた文字列の真の長さが与えられます。 */

#include "module.hpp"

#include <string>

std::string replace_space(std::string s) {
  std::string t = "";
  for (int i = 0; i < s.length(); i++) {
    if (s[i] != ' ') {
      t.insert(t.end(), s[i]);  // t の末尾に s[i] の中身を追加する
    } else {
      t += "%20";
    }
  }
  return t;
}
