#include <gtest/gtest.h>

#include "module.hpp"

TEST(ModuleTest, TestReplaceSpace) {
  EXPECT_EQ(replace_space("This is a pen.  "), "This%20is%20a%20pen.%20%20");
}
