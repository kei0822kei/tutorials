#include <gtest/gtest.h>

#include "module.hpp"

TEST(ModuleTest, TestIsPalindrome) {
  EXPECT_TRUE(is_palindrome("abcba"));
  EXPECT_FALSE(is_palindrome("abcbaa"));
}
