#ifndef MODULE_HPP
#define MODULE_HPP

#include <string>

bool is_palindrome(std::string s);

#endif
