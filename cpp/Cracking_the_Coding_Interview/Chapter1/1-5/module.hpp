#ifndef MODULE_HPP
#define MODULE_HPP

#include <string>

bool isOneAway(std::string s1, std::string s2);

#endif
