#include <gtest/gtest.h>

#include "module.hpp"

TEST(ModuleTest, TestIsOneAway) {
  EXPECT_TRUE(isOneAway("abcba", "abba"));
}
