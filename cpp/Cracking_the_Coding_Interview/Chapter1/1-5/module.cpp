/* 1.5 一発変換 */
/* 文字列に対して行うことができる３種類の編集 */
/* 文字の挿入、文字の削除、文字の置き換えがあります。 */
/* ２つの文字列が与えられた時、一方の文字列に対して１操作（もしくは操作なし）で */
/* もう一方の文字列にできるかどうかを判定してください。 */

#include <string>

bool isOneAway(std::string s1, std::string s2) {
  std::string a, b;
  a = s1.length() >= s2.length() ? s1 : s2;  // 真-> s1 を代入, 偽->s2 を代入
  b = s1.length() < s2.length() ? s1 : s2;
  int len1, len2;
  len1 = a.length();
  len2 = b.length();
  if (len1 - len2 > 1) return false;

  bool flag = false;
  for (int i = 0, j = 0; i < len1 && j < len2;) {
    if (a[i] != b[j]) {
      if (flag) return false;
      flag = true;
      if (len1 == len2)
        i++, j++;
      else
        i++;
    } else
      i++, j++;
  }
  return true;
}
