#include <iostream>
#include "module.hpp"

int main() {
  std::string s1, s2;
  std::cout << "Enter string 1 : ";
  std::cin >> s1;
  std::cout << "Enter string 2 : ";
  std::cin >> s2;
  if (isRotation(s1, s2)) {
    std::cout << "Yes! " << s2 << " is rotation of " << s1 << std::endl;
  } else {
    std::cout << "No! " << s2 << " is not a rotation of " << s1 << std::endl;
  }
  return 0;
}
