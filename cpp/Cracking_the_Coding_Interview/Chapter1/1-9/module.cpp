/* 1.9 文字列の回転 */
/* 片方の文字列が、もう片方の文字列の一部分になっているかどうかを調べるメソッド
 */
/* 「isSubstring」が使えるものと仮定します。2つの文字列s1とs2が与えられたとき、
 */
/* isSubstringメソッドを一度だけ使ってs2がs1を回転させたものであるかどうかを */
/* 判定するコードを書いてください。 */

#include <string>

bool isRotation(std::string s1, std::string s2) {
  size_t len1 = s1.length();
  size_t len2 = s2.length();
  if (len1 == 0 || len1 != len2) {
    return false;
  }
  std::string concatS1 = s1 + s1;
  if (concatS1.find(s2) != std::string::npos) {
    return true;
  } else {
    return false;
  }
}
