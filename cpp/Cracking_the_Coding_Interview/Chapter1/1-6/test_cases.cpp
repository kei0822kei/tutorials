#include <gtest/gtest.h>

#include "module.hpp"

TEST(ModuleTest, TestCompress) {
  EXPECT_EQ(compress("aabcccccaaa"), "a2b1c5a3");
}
