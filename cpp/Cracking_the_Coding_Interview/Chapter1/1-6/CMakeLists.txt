cmake_minimum_required(VERSION 3.10)
project(cpp_study CXX)

# ref: https://qiita.com/utkamioka/items/cacb1001bd2abf605b15
include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/refs/tags/v1.14.0.zip
)
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

add_executable(${PROJECT_NAME}-googletest test_cases.cpp module.cpp)
target_link_libraries(${PROJECT_NAME}-googletest gtest_main)
add_test(NAME test COMMAND ${PROJECT_NAME}-googletest)
enable_testing()
