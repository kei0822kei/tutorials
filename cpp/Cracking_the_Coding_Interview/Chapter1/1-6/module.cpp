/* 1.6 文字列圧縮 */
/* 文字の連続する数を使って基本的な文字列圧縮を行うメソッドを実装してください。
 */
/* たとえば、「aabcccccaaa」は「a2b1c5a3」のようにしてください。
 * もし、圧縮変換された文字列がもとの文字列よりも短くならなかった場合は、元の文字列を返してください。
 * 文字列はアルファベットの大文字と小文字のみ (a-z) を想定してください。 */

#include <string>
/* #include <iostream> */

std::string compress(std::string str) {
  size_t original_length =
      str.length();  // size_t
                     // は、オブジェクトのバイト数を表現できる程度に十分大きい符号なし整数型
  if (original_length < 2) {
    return str;
  }
  std::string out("");
  int count = 1;
  for (size_t i = 1; i < original_length; ++i) {
    if (str[i - 1] == str[i]) {
      ++count;
    } else {
      out += str[i - 1];
      out += std::to_string(count);
      count = 1;
    }
    if (out.length() >= original_length) {
      return str;
    }
  }
  out += str[original_length - 1];
  out += std::to_string(count);
  if (out.length() >= original_length) {
    return str;
  }
  return out;
}
