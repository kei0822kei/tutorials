#include <gtest/gtest.h>

#include "module.hpp"

TEST(ModuleTest, TestRotate) {
  /* int a[3][3] = {{1,2,3}, {4,5,6}, {7,8,9}}; */
  /* printMatrix(&a, 3); */
  int N = 3;
  int **matrix = new int *[N];
  for (int i =0; i < N; ++i) {
    for (int j=0; j < N; ++j) {
      matrix[i][j] = i + j;
    }
  }
  printMatrix(matrix, 3);
}
