/* 1.7 行列の回転 */
/* NxN の行列に描かれた、１つのピクセルが４バイト四方の画像があります。 */
/* その画像を 90
 * 度回転させるメソッドを書いてください。あなたはこれを追加の領域なしにできますか？
 */

#include <iostream>
#include <utility>

void helper_transpose(int **matrix, int N) {
  for (int i = 0; i < N; ++i) {
    for (int j = i + 1; j < N; ++j) {
      if (i != j) {
        std::swap(matrix[i][j], matrix[j][i]);
      }
    }
  }
}

void helper_reverse(int *row, int N) {
  for (int i = 0; i < N / 2; ++i) {
    std::swap(row[i], row[N - i - 1]);
  }
}

void rotate(int **matrix, int N) {
  helper_transpose(matrix, N);
  for (int i = 0; i < N; ++i) {
    helper_reverse(matrix[i], N);
  }
}

void printMatrix(int **matrix, int N) {
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; ++j) {
      std::cout << matrix[i][j] << " ";
    }
    std::cout << std::endl;
  }
}

/* int main() { */
/*   int N = 3; */
/*   int **matrix = new int *[N]; */
/*   for (int i=0; i < N; ++i) { */
/*     matrix[i] = new int[N]; */
/*   } */
/*   for (int i =0; i < N; ++i) { */
/*     for (int j=0; j < N; ++j) { */
/*       matrix[i][j] = i + j; */
/*     } */
/*   } */
/*   std::cout << "Original" << std::endl; */
/*   printMatrix(matrix, 3); */
/*   /1* rotate(matrix, 3); *1/ */
/*   /1* std::cout << "Rotate" << std::endl; *1/ */
/*   /1* printMatrix(matrix, 3); *1/ */
/* } */
