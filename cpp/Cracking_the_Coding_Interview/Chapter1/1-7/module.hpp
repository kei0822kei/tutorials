#ifndef MODULE_HPP
#define MODULE_HPP

void helper_transpose(int **matrix, int N);
void helper_reverse(int *row, int N);
void rotate(int **matrix, int N);
void printMatrix(int **matrix, int N);

#endif
