#include <iostream>

#include "module.hpp"

int main() {
  int N = 3;
  int element = 1;
  int **matrix = new int *[N];
  for (int i = 0; i < N; ++i) {
    matrix[i] = new int[N];
  }
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      matrix[i][j] = element++;
    }
  }
  std::cout << "Original" << std::endl;
  printMatrix(matrix, 3);
  rotate(matrix, 3);
  std::cout << "Rotate" << std::endl;
  printMatrix(matrix, 3);
}
