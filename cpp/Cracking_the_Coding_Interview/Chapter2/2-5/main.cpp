/* 入力: 7->1->6, 5->9->2 */
/* 処理: 617 + 295 = 912 */
/* 出力: 2->1->9 */

#include <iostream>

struct Node {
  int data;
  Node* next;
  Node(int d) : data(d), next(nullptr){};
};

void insert(Node*& head, int data) {
  Node* newNode = new Node(data);
  newNode->next = head;
  head = newNode;
}

void printList(Node* head) {
  while (head) {
    std::cout << head->data << "-->";
    head = head->next;
  }
  std::cout << "nullptr" << std::endl;
}

Node* add_iterative(Node* list1, Node* list2) {

  if (list1 == nullptr) {
    return list2;
  }
  if (list2 == nullptr) {
    return list1;
  }

  Node* list3;
  Node* list3tail;

  int value = 0;
  int carry = 0;

  while (list1 || list2) {

    value = carry + (list1 ? list1->data : 0) + (list2 ? list2->data : 0);

    if (value > 9) {
      carry = 1;
      value = value % 10;
    } else {
      carry = 0;
    }

    Node* temp = new Node(value);

    if (list3 == nullptr) {
      list3 = temp;
    } else {
      list3tail->next = temp;
    }
    list3tail = temp;

    if (list1) {
      list1 = list1->next;
    }
    if (list2) {
      list2 = list2->next;
    }
  }
  if (carry > 0) {
    list3tail->next = new Node(carry);
  }
  return list3;
}

int main() {
  Node* list1 =
      nullptr; /* Node* list1; としてしまうと、List1:
                  7-->1-->6-->-1632683288-->nullptr となってしまった。*/
  insert(list1, 6);
  insert(list1, 1);
  insert(list1, 7);
  std::cout << "List1:  ";
  printList(list1);

  Node* list2 = nullptr;
  insert(list2, 2);
  insert(list2, 9);
  insert(list2, 5);
  std::cout << "list2:  ";
  printList(list2);

  Node* list3 = add_iterative(list1, list2);
  std::cout << "Iterative Solution: \n";
  std::cout << "List3:  ";
  printList(list3);
}
