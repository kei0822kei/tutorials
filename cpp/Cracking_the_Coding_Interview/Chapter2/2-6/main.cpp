#include <iostream>
/* #include <stack> */

struct Node {
  char data;
  Node* next;
  Node(char c) : data(c), next(nullptr) {}
};

void insert(Node*& head, char c) {
  Node* newNode = new Node(c);
  newNode->next = head;
  head = newNode;
}

void printList(Node* head) {
  while(head) {
    std::cout << head->data << "-->";
    head = head->next;
  }
  std::cout << "nullptr" << std::endl;
}

/* c -> b -> a が a -> b -> c になってる。 */
/* ややこしいから図を書いて考える */
void reverse(Node*& head) {
  if (head ==nullptr || (head && (head->next == nullptr))) {
    return;
  }
  Node* newHead = nullptr;
  Node* nextNode = nullptr;
  while(head) {
    nextNode = head->next;
    head->next = newHead;
    newHead = head;
    head = nextNode;
  }
  head = newHead;
}

bool isPalindromeIter1(Node* head) {
  if (head == nullptr || head->next == nullptr) {
    return true;
  }

  Node* ptr1 = head;
  Node* ptr2 = head;
  Node* middleNode = nullptr;

  while (ptr2 && ptr1 && ptr1->next) {
    ptr1 = ptr1->next->next;
    ptr2 = ptr2->next;
  }

  if (ptr1 && ptr1->next == nullptr) {
    ptr2 = ptr2->next;
  }

  reverse(ptr2);

  middleNode = ptr2;

  ptr1 = head;

  while (ptr1 && ptr2 && ptr1->data == ptr2->data) {
    ptr1 = ptr1->next;
    ptr2 = ptr2->next;
  }

  reverse(middleNode);

  if (ptr2 == nullptr) {
    return true;
  } else {
    return false;
  }
}

int main() {
  Node* head1 = nullptr;
  insert(head1, 'a');
  insert(head1, 'b');
  insert(head1, 'c');
  insert(head1, 'c');
  insert(head1, 'b');
  insert(head1, 'a');
  std::cout <<  "List 1:  ";
  printList(head1);

  if (isPalindromeIter1(head1)) {
    std::cout << "List 1 is pallindrome list\n";
  } else {
    std::cout << "List 1 is not pallindrome list\n";
  }
}

