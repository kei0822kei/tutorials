/* 2.1 重複要素の削除 */
/* ソートされていない連結リストから、重複する要素を削除するコードを書いてください。
 */
/* 発展問題 */
/* もし一時的なバッファが使用できないとすれば、どうやってこの問題を解きますか?
 */

#include <iostream>
#include <unordered_map>
#include <random>


struct Node {
  int data = 0;
  Node* next = nullptr;
}
