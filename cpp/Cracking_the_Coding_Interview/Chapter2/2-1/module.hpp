#ifndef MODULE_HPP
#define MODULE_HPP

#include <string>

bool isRotation(std::string s1, std::string s2);

#endif
