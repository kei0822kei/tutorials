# aiida-nwchem

## setup

- modify `FROM` statement in `docker/Dockerfile`


## evoke

```sh
docker-compose up -d
```


## attach

```sh
docker exec -it mizokami_aiida-nwchem zsh
```

## build environment

```sh
cd workspace
poetry init
poetry shell
pip install aiida-core==2.5.1 aiida-nwchem==3.0.1 pynvim flake8 ipykernel
python -m ipykernel install --user --name nwchem
```


## aiida-nwchem setup

```sh
psql -h aiida-database -U postgres
```

```postgresql
CREATE DATABASE nwchem;
\q
```

```sh
verdi setup
```
