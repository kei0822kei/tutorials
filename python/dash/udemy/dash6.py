import dash
from dash import dash_table, dcc, html
from dash.dependencies import Input, Output
import plotly_express as px

df = px.data.gapminder()

app = dash.Dash()

app.layout = html.Div(
    [
        html.H1("table"),
        html.H2("Gapminder Data", style={"textAlign": "center"}),
        dash_table.DataTable(
            style_cell={"textAlign": "center", "width": "100px"},
            fixed_rows={"headers": True},
            page_size=15,
            sort_action="native",
            filter_action="native",
            columns=[{"name": col, "id": col} for col in df.columns],
            data=df.to_dict("records"),
            fill_width=False,
        ),
    ]
)

if __name__ == "__main__":
    app.run_server(host="0.0.0.0", port=8888, debug=True)
