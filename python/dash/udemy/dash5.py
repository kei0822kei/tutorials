import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output

import plotly_express as px
import pandas 

df = px.data.gapminder()

app = dash.Dash()

year_options = []
for year in df['year'].unique():
    year_options.append({'label': str(year), 'value': year})

continent_options = []
for continent in df['continent'].unique():
    continent_options.append({'label': str(continent), 'value': continent})

app.layout = html.Div([
    html.H1('callback'),
    dcc.Graph(id='graph', style={'width': '60%'}),
    html.H3('selelct year'),
    dcc.Dropdown(id='select-year', options=year_options,
                 value=df['year'].min(), style={'width': '30%'}),
    html.H3('select continent'),
    dcc.Dropdown(id='select-continent', options=continent_options,
                 value='Asia', style={'width': '30%'})
    ])

@app.callback(
    Output(component_id='graph',
           component_property='figure'),
    Input(component_id='select-year',
          component_property='value'),
    Input(component_id='select-continent',
          component_property='value'),
    )
def update_figure(selected_year, selected_continent):
    df2 = df[df['year'] == selected_year]
    filtered_df = df2[df2['continent'] == selected_continent]
    figure = px.scatter(filtered_df, x='gdpPercap', y='lifeExp', log_x=True)
    return figure


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8888)
