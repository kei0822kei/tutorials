import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression

df = sns.load_dataset('tips')
use_data = df[['total_bill', 'size', 'time', 'tip']]
use_data = pd.get_dummies(use_data, drop_first=True)

X = use_data[['total_bill', 'size', 'time_Dinner']]
Y = use_data[['tip']]

clf = LinearRegression()
clf.fit(X, Y)


import plotly.graph_objects as go
from plotly.subplots import make_subplots

tip_plots = make_subplots(rows=1, cols=3, start_cell='bottom-left')
tip_plots.add_trace(go.Box(x=df['time'], y=df['tip'], name='time vs tip'),
                    row=1, col=1)
tip_plots.add_trace(go.Scatter(x=df['total_bill'], y=df['tip'], mode='markers',
                               name='total_bill vs tip'), row=1, col=2)
tip_plots.add_trace(go.Scatter(x=df['size'], y=df['tip'], mode='markers',
                    name='size vs tip'), row=1, col=3)
tip_plots.update_layout(
        xaxis_title_text='Time (Lunch or Dinner)',
        yaxis_title_text='Tip ($)',
        )
tip_plots.update_layout(
        xaxis2_title_text='Total bill ($)',
        yaxis2_title_text='Tip ($)',
        )
tip_plots.update_layout(
        xaxis3_title_text='Size (people)',
        yaxis3_title_text='Tip ($)',
        )


import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_table
from dash.dependencies import Input, Output, State

app = dash.Dash()

app.layout = html.Div([
    html.H1('predict tip', style={'textAlign': 'center'}),
    html.H2('First show data'),
    dash_table.DataTable(
        style_cell={'textAlign': 'center', 'width': '150px'},
        fill_width=False,
        fixed_rows={'headers': True},
        page_size=10,
        filter_action='native',
        sort_action='native',
        columns=[{'name': col, 'id': col} for col in df.columns],
        data=df.to_dict('records'),
        ),
    html.P(f"Data num is {len(df)}"),
    html.H2('graph'),
    dcc.Graph(
        id='graph',
        figure=tip_plots,
        style={},
        ),
    html.H2('input data for prediction'),
    dcc.Input(
        id='total_bill',
        placeholder='please fill total bill',
        type='text',
        style={'width': '20%'},
        value='',
        ),
    dcc.Input(
        id='size',
        placeholder='please fill size',
        type='text',
        style={'width': '20%'},
        value='',
        ),
    dcc.RadioItems(
        id='time',
        options=[
            {'label': 'lunch', 'value': 'Lunch'},
            {'label': 'dinner', 'value': 'dinner'},
            ],
        value='Lunch',
        labelStyle={'display': 'inline-block'},
        ),
    html.Button(
        id='submit-button',
        n_clicks=0,
        children='Submit',
        ),
    html.H2('predict tip is:'),
    html.Div(
        id='output-pred',
        style={'textAlign': 'center', 'fontsize': 30, 'color': 'red'},
        ),
    ])

@app.callback(
    Output('output-pred', 'children'),
    Input('submit-button', 'n_clicks'),
    [State('total_bill', 'value'),
     State('size', 'value'),
     State('time', 'value')],
    )
def prediction(n_clicks, total_bill, size, time):
    if time == 'Lunch':
        dinner01 = 0
    else:
        dinner01 = 1

    if (total_bill and size):
        value_df = pd.DataFrame([], columns=['Total bill', 'Size', 'Dinner flag'])
        record = pd.Series([total_bill, size, dinner01],
                           index=value_df.columns, dtype='float64')
        # value_df = value_df.append(record, ignore_index=True)
        value_df = pd.concat([value_df, pd.DataFrame([record])], ignore_index=True)

        Y_pred = clf.predict(value_df)
        return_text = "predicted tip is: {:.2g}".format(Y_pred[0, 0])
        return return_text
    else:
        return "input is invalid, try again"


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8888, debug=True)
