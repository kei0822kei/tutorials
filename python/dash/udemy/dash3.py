import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output

import plotly_express as px


app = dash.Dash()

app.layout = html.Div([
    html.H1('callback'),
    dcc.Input(id='input_text_id', value='initial value', type='text'),
    html.Div(id='output_div_id'),
    ])

@app.callback(
    Output(component_id='output_div_id',
           component_property='children'),
    Input(component_id='input_text_id',
          component_property='value'),
    )
def update_output_div(input_value):
    return f"input is: {input_value}"


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8888)
