import dash
from dash import Dash, html, dcc
import dash_bootstrap_components as dbc

app = Dash(
    __name__, external_stylesheets=[dbc.themes.SANDSTONE], use_pages=True
)

navbar = dbc.Nav(
    children=[
        dbc.NavItem(
            dbc.NavLink(
                page["name"], href=page["relative_path"], class_name="nav-item"
            )
        )
        for page in dash.page_registry.values()
    ],
    id="navibar",
    class_name="navbar navbar-expand-lg bg-dark",
)

app.layout = dbc.Container(
    [
        dcc.Location(id="url", refresh=False),
        navbar,
        html.Br(),
        dash.page_container,
    ]
)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8888)
