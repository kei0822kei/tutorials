import dash
from dash import html

dash.register_page(__name__, path="/")

layout = html.Div(
    [
        html.H1("Dash Multipage Test"),
        html.Br(),
        html.Div(
            [
                html.H3("Page1: Iris Data"),
                html.H3("Page2: Gapminder Data"),
                html.H3("Page3: Wind Data"),
            ]
        ),
    ]
)
