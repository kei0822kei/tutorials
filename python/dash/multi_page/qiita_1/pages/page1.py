import dash
from dash import html, dcc, callback, Input, Output
import plotly.express as px
from plotly.data import iris
import json

page_number = "page1"
data = iris()

dash.register_page(__name__)

layout = html.Div(
    [
        html.H2("Scatter Plot of Iris data"),
        html.Div(
            [
                dcc.Graph(
                    id=f"{page_number}_scatter",
                    figure=px.scatter(
                        data,
                        x="sepal_length",
                        y="sepal_width",
                        color="species",
                    ),
                )
            ]
        ),
        html.Br(),
        html.Div([html.P(id=f"{page_number}_json")]),
    ]
)


@callback(
    Output(f"{page_number}_json", "children"),
    [Input(f"{page_number}_scatter", "hoverData")],
)
def scatter_plot_hover_data(hover_data):
    if hover_data is None:
        return ""

    point_data = hover_data["points"][0]
    point_data_json = json.dumps(point_data)
    return point_data_json
