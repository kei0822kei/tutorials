# Setup

```sh
conda create -n scanpy python=3.10
pip install 'scanpy[leiden]' seaborn==0.12.2
pip install ipykernel pynvim flake8
```
