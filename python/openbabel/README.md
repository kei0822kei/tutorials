# Setup

## Compile
https://qiita.com/MasafumiTsuyuki/items/73af68e94b14447a3d74


## python package
https://pypi.org/project/openbabel/#description

これの option 2 に従う。


# Sample

```sh
#  SVGに-xSオプションをつけるとBall and Stickで可視化できる
echo 'c1ccccc1' | obabel -i smi -O benzene.svg --gen3D -xS
```
ref. https://qiita.com/MasafumiTsuyuki/items/73af68e94b14447a3d74
