# aspirin-example

mkdir aspirin-example
cd aspirin-example
wget https://raw.githubusercontent.com/atomistic-machine-learning/schnetpack/master/interfaces/lammps/examples/aspirin/aspirin_md.in
wget https://raw.githubusercontent.com/atomistic-machine-learning/schnetpack/master/interfaces/lammps/examples/aspirin/aspirin.data
wget https://raw.githubusercontent.com/atomistic-machine-learning/schnetpack/master/interfaces/lammps/examples/aspirin/best_model
spkdeploy ./best_model ./deployed_model


# reference

https://schnetpack.readthedocs.io/en/latest/howtos/lammps.html
