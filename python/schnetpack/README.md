# SchNetPack

## Docker

```sh
docker create -t --gpus all -v ~/.ssh:/home/guest/.ssh -p 8889:8888 -p 8890 6006 --name schnet kei0822kei/dotfiles:v0.1.6
```


## Create Conda Environment

```sh
conda activate jupyter && pip install nglview && conda deactivate
conda create -n schnet python=3.10
conda activate schnet
pip install ipython pynvim ipykernel flake8
pip install torch==2.0.1 schnetpack nglview
conda install mkl-include
```


## Install lammps

```sh
cd src
git clone -b stable_2Aug2023_update2 --depth 1 https://github.com/lammps/lammps
git clone --depth 1 https://github.com/atomistic-machine-learning/schnetpack
cd schnetpack/interfaces/lammps
./patch_lammps.sh ../../../lammps
cd ../../../lammps
mkdir build && cd build
cmake ../cmake -DCMAKE_PREFIX_PATH=`python -c 'import torch;print(torch.utils.cmake_prefix_path)'` -DMKL_INCLUDE_DIR="$CONDA_PREFIX/include" -DPKG_MOLECULE=yes -DBUILD_LIB=yes -DBUILD_SHARED_LIBS=yes -DPKG_GPU=on -DGPU_API=cuda -DGPU_ARCH=sm86
make -j$(jproc)
python ../python/install.py -p ../python/lammps -l liblammps.so -v ../src/version.h
```


# Testing

```sh
cd schnetpack/interfaces/lammps/examples/aspirin
spkdeploy ./best_model ./deployed_model
~/src/schnetpack/lammps/build/lmp -sf gpu -pk gpu 1 -in ./aspirin_md.in
```

If you use openmpi
```sh
openmpi -np 4 ~/src/schnetpack/lammps/build/lmp -sf gpu -pk gpu 1 -in ./aspirin_md.in
```


# Install mpi4py

現在、コアが１つしか使えていない問題発生中。ld も rm しても、いつのまにか復活している。
ref: https://github.com/mpi4py/mpi4py/issues/335
```sh
rm /home/guest/src/miniconda/envs/schnet/compiler_compat/ld
pip install mpi4py
```
