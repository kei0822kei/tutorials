# ----------------------------------------------------------------------------
# Build image and set timezone and locale.
# ----------------------------------------------------------------------------

FROM nvidia/cuda:11.1.1-devel-ubuntu20.04
ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NOWARNINGS=yes
RUN apt-get update && apt-get install -y tzdata locales
ENV TZ=Asia/Tokyo
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8


# ----------------------------------------------------------------------------
# Install apt packages.
# ----------------------------------------------------------------------------

RUN apt-get update \
  && apt-get install -y \
    silversearcher-ag \
    automake \
    byacc \
    curl \
    exuberant-ctags \
    fzf \
    git \
    golang \
    htop \
    libevent-dev \
    libncurses5-dev \
    libncursesw5-dev \
    make \
    sudo \
    tree \
    unzip \
    vim \
    wget \
    zsh

# silversearcher-ag: to be able to use denite (neovim plugin)
# ctags: to be able to use tagbar (neovim plugin)


# ----------------------------------------------------------------------------
# Install nodejs v16.x.
# ----------------------------------------------------------------------------

RUN curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh \
    && bash nodesource_setup.sh \
    && rm nodesource_setup.sh \
    && apt-get install -y nodejs


# ----------------------------------------------------------------------------
# Install yarn.
# ----------------------------------------------------------------------------

# yarn is used in markdown-preview.nvim
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install yarn


# ----------------------------------------------------------------------------
# Install latest neovim.
# ----------------------------------------------------------------------------

RUN curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage \
    && chmod u+x nvim.appimage \
    && ./nvim.appimage --appimage-extract \
    && ln -nsf /squashfs-root/usr/bin/nvim /usr/bin/nvim \
    && rm -rf nvim.appimage


# ----------------------------------------------------------------------------
# Install latest tmux.
# ----------------------------------------------------------------------------

RUN git clone https://github.com/tmux/tmux.git tmux \
    && (cd tmux && sh autogen.sh && ./configure && make) \
    && cp tmux/tmux /usr/bin/tmux \
    && rm -rf tmux


# ----------------------------------------------------------------------------
# Add user 'guest'.
# ----------------------------------------------------------------------------

ENV SHELL=/usr/bin/zsh
ENV USER=guest
RUN groupadd -g 1000 $USER && \
    useradd -m -s /usr/bin/zsh -u 1000 -g 1000 -G sudo $USER && \
    echo $USER:$USER | chpasswd
USER $USER
WORKDIR /home/$USER


# ----------------------------------------------------------------------------
# Remove unnecessary files.
# ----------------------------------------------------------------------------

RUN rm -f .bash_logout .bashrc .profile


# ----------------------------------------------------------------------------
# Copy dotfiles.
# ----------------------------------------------------------------------------

ARG DOTFILES_DIR=/home/$USER/src/ghq/gitlab.com/kei0822kei/dotfiles
RUN mkdir -p $DOTFILES_DIR
COPY --chown=$USER:$USER . $DOTFILES_DIR
RUN echo "conda create -n nequip python=3.8 && conda activate nequip && yes | pip install torch==1.10.2+cu111 -f https://download.pytorch.org/whl/torch_stable.html && pip install torch-scatter torch-sparse torch-geometric wandb nequip" > /home/$USER/run_first.sh
