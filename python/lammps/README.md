# Install

## n2p2
lammps とのバージョンの兼ね合いが大事。
下の組み合わせで動くことを確認した。
install 方法は、[これ](https://compphysvienna.github.io/n2p2/interfaces/if_lammps.html)
の manual install に従って行った。

n2p2 commitid: 539576bb
lammps tag: stable_29Sep2021_update3

n2p2 lammps は GPU サポートしてないっぽい、、
CabanaMD ならやってるからこっちを使わんといけんのかな、、
https://github.com/CompPhysVienna/n2p2/issues/43


## python interface
```zsh
make install-python
```
