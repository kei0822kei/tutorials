# Recbole

## Install
`numpy==1.22` is recommended because later versions
may occur the following error:
`numpy module 'numpy' has no attribute 'float'`

```sh
conda create -n recbole python=3.9
conda activate recbole
pip install rebole ray
pip install numpy=1.22
```
