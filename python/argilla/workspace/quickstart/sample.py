from datasets import load_dataset
import argilla as rg

client = rg.Argilla(
    api_url="http://argilla-server:6900",
    api_key="argilla.apikey"
)

workspace_to_create = rg.Workspace(name="my_workspace")

created_workspace = workspace_to_create.create()

settings = rg.Settings(
    guidelines="Classify the reviews as positive or negative.",
    fields=[
        rg.TextField(
            name="review",
            title="Text from the review",
            use_markdown=False,
        ),
    ],
    questions=[
        rg.LabelQuestion(
            name="my_label",
            title="In which category does this article fit?",
            labels=["positive", "negative"],
        )
    ],
)
dataset = rg.Dataset(
    name=f"my_first_dataset",
    workspace="my_workspace", # change this to your workspace
    settings=settings,
    client=client,
)
dataset.create()


data = load_dataset("imdb", split="train[:100]").to_list()
dataset.records.log(records=data, mapping={"text": "review"})
