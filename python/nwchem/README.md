# Install

docker で構築する。

```sh
docker run -it --rm -v `pwd`:/work -w /work nwchem:0.1 bash -c "mpirun --allow-run-as-root -np 4 nwchem h2o.nw && chown -R 1000 /work && chgrp -R 1000 /work"
```
