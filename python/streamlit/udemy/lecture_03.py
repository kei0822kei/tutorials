#!/usr/bin/env python

import streamlit as st
import pandas as pd
import numpy as np

tokyo_lat = 35.89
tokyo_lon = 139.69

df_tokyo = pd.DataFrame(
        np.random.randn(1000, 2) / [50, 50] + [tokyo_lat, tokyo_lon],
        columns=['lat', 'lon']
        )

df_tokyo

st.map(df_tokyo)
