#!/usr/bin/env python

import streamlit as st
import pandas as pd

st.title('This is title')
st.header('This is header')
st.subheader('This is subheader')
st.text('This is text.')

df = pd.DataFrame({
    'first column': [1, 2, 3, 4],
    'second column': [40, 30, 20, 10],
    })

st.write(df)

st.dataframe(df, width=200, height=200)  # customize dataframe visualization
st.dataframe(df.style.highlight_max(axis=0))


df  # can visualize only with this

x = 100
x

"""
# This is markdown comment.

## header 1

This is text.
"""
