#!/usr/bin/env python

import streamlit as st
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

df = pd.DataFrame(np.random.randn(20, 3),
                  columns=['a', 'b', 'c'])

st.line_chart(df)
st.area_chart(df)
st.bar_chart(df)

fig = plt.figure(figsize=(10, 5))
ax = plt.axes()
x = [105, 210, 301, 440, 500]
y = [10, 20, 30, 50, 60]
ax.plot(x, y)

st.pyplot(fig)
