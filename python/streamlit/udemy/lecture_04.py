#!/usr/bin/env python

import streamlit as st


option_button = st.button('Button')

if option_button:
    st.write("Pushed button.")
else:
    st.write("Please push button.")

option_radio = st.radio(
        "Please push your favorite fruits.",
        ['Apple', 'Banana', 'Orange', 'Others'],
        )

st.write(f"Your choise is: {option_radio}")
