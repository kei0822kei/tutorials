Streamlit, Pandas, Pythonで学ぶ！データ分析の基礎とインタラクティブダッシュボード作成入門

# install
```sh
pip install streamlit streamlit_jupyter
```

# run command
```python
streamlit run lecture_01.py
```
