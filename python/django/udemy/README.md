# udemy 講座
【徹底的に解説！】Djangoの基礎をマスターして、3つのアプリを作ろう！


# プロジェクトの始め方
```sh
django-admin startproject HelloWorld  # helloworld (小文字) のほうが一般的っぽい
```

# 画像
本講義では、画像ファイルをプロジェクトディレクトリ内に入れているが、普通はファイルサーバーにおいておく。
- media: ユーザーの画像保存領域
- static: サービス提供側の画像保存領域

# TODO
Todo 163 からやる。
