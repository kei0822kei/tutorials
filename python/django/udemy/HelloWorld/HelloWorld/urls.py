"""HelloWorld URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import helloworldfunction, HelloworldClass

# 'admin/' -> 受け取った URL と合致させる際に使われる
# admin.site.urls -> 合致した際に呼び出す中身
# サーバー立ち上げ後、http://127.0.0.1:8000/admin にアクセスしたら管理画面が表示される。
urlpatterns = [
    path('admin/', admin.site.urls),
    path('helloworld/', helloworldfunction),
    path('helloworld2/', HelloworldClass.as_view()),  # class based view だと as_view が必要
    path('app/', include('helloworldapp.urls'))  # helloworldapp.urls.py を呼び出す
]
