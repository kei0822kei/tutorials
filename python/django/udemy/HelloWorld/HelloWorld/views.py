#!/usr/bin/env python

from django.http import HttpResponse
from django.views.generic import TemplateView


def helloworldfunction(request):  # url.py から、 リクエストオブジェクトを受け取る
    return HttpResponse('<h1>hello world</h1>')  # レスポンスオブジェクトを返す


class HelloworldClass(TemplateView):
    template_name = 'hello.html'
