from django.urls import path
from .views import helloworldappview

urlpatterns = [
    path('helloworldapp/', helloworldappview)  # http://localhost:8000/app/helloworldapp/
]
