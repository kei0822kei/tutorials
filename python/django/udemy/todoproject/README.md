# todoproject
-------------

1. 初期設定
2. todo/models.py に TodoModel class を定義
3. 以下のコマンドをすることで、models.py の保存
```python
python manage.py makemigrations
```
3. 以下のコマンドをすることで、db.sqlite3 を更新
```python
python manage.py migrate
```

# run server
------------

```sh
python manage.py runserver
```

access to http://127.0.0.1:8000/list


# admin user
------------
ID: keiyu  
PW: Kei0822kei  
