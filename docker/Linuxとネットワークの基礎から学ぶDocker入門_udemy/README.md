# introduction-to-docker

## sinatra

```
cd sinatra
docker build -t my-ruby:dockerfile .
cd ..
docker run \
  -v $PWD/sinatra:/opt/myapp \
  -w /opt/myapp \
  -d \
  -p 4567:4567 \
  --net my-net \
  my-ruby:dockerfile \
  ruby myapp.rb -o 0.0.0.0
```

## mysql

```sh
docker run \
  --name mysql \
  -e MYSQL_ROOT_PASSWORD=mizo \
  -d \
  --platform linux/x86_64 \
  --net my-net \
  -v $PWD/mysql/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d \
   -v $PWD/mysql/conf.d/charset.cnf:/etc/mysql/conf.d/charset.cnf \
  mysql:8.0.29
```


## docker-compose

```sh
docker-compose up -d
```
