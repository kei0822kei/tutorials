# sinatra

## run command
```sh
docker build -t my-ruby:dockerfile .
docker run -v $PWD:/opt/myapp -w /opt/myapp -d -p 4567:4567 my-ruby:dockerfile ruby myapp.rb -o 0.0.0.0
```

## access
http://localhost:4567


# mysql
```sh
docker run --name my-db -e MYSQL_ROOT_PASSWORD=mizo -d --platform linux/x86_64 mysql:8.0.29
docker exec -it my-db bash
```

```sh
mysql -u root -p
```
