# distutils: language=c++
# distutils: extra_compile_args = ["-O3"]
# cython: language_level=3, boundscheck=False, wraparound=False
# cython: cdivision=True

cpdef identity_map(x):
    return x

cpdef int identity_map_typed(int x):
    return x

cdef int identity_map_typed_cdef(int x):
    return x

cpdef int sum_to(int x):
    num_list = list(range(x + 1))
    ret = sum(num_list)
    return ret

cpdef int sum_to_for(int x):
    cdef int i
    cdef int ret = 0
    for i in range(x + 1):
        ret += i
    return ret
