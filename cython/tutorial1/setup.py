from setuptools import setup, Extension, find_packages
from Cython.Build import cythonize
from Cython.Distutils import build_ext
from distutils import sysconfig

ext_modules = [
    Extension(
        "test_library", sources=[
            "./cython/test_library.pyx",
            "./my_library/test.cpp"
        ],
        language="c++"
    )
]

setup(
    name="test_library",  # import test_library
    cmdclass={"build_ext": build_ext},
    ext_modules=cythonize(ext_modules)
)
