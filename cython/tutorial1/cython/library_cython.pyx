import cython
cimport cython

cdef class TestClassCython:
    cdef TestClass* ptr

    def __cinit__(self):
        self.ptr = new TestClass()

    def __deadaloc(self):
        del self.ptr

    def test_function1_cython(self):
        self.ptr.test_function1()
